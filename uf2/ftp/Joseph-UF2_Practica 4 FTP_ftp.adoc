=== Practica servidor FTP

1. Preparacio de la maquina TYR

- Configuracio netplan TYR

image::Screenshot_1.png[]

- Taules enrutament TYR

image::Screenshot_2.png[]

2. Instal·lador del servidor FTP a TYR

- Transaccio a partir de ftp entre heimdall i tyr

image::Screenshot_3.png[]

3. Configuracio de l'enrutament

- Transaccio entre ordinador anfitrio i tyr

image::Screenshot_4.png[]

- Configuracio iptables (THOR)

image::Screenshot_5.png[]

- Connexio FTP entre PC1 i TYR

image::Screenshot_6.png[]

4. Configuracio de l'acces anonim

- Creacio directori anonymous

image::Screenshot_7.png[]

- Configuracio arxiu /etc/vsftpd.conf

image::Screenshot_8.png[]

- Transaccio utilitzan anonim

image::Screenshot_9.png[]

5. Separacio de la configuracio FTP en xarxes

- Canvis arxiu de configuracio

 Xarxa interna
 
image::Screenshot_10.png[]

 Xarxa externa
 
image::Screenshot_11.png[]

 Host.allow

image::Screenshot_12.png[]

- Comprovacio acces interior

image::Screenshot_13.png[]

- Comprovacio acces exterior

image::Screenshot_14.png[]

6. Configuracio de l'espai compartit

- Creacio usuari ftpuser

image::Screenshot_15.png[]

- Donar contrasenya a ftpuser

image::Screenshot_16.png[]

- Conectar a ftp amb ftpuser

image::Screenshot_17.png[]

- Reiniciar servidor vsftpd

image::Screenshot_18.png[]

- Comanda per treure permis d'escriptura al home de l'usuari ftpuser

image::Screenshot_19.png[]

- Comprovar que ftpuser si pot conectar al ftp, pero el usuari habitual no

image::Screenshot_20.png[]

7. Configuracio de l'espai per a cada usuari

- Canvis fet al arxiu de configuracio de la xarxa interna

image::Screenshot_21.png[]

- Comprovacio de que podem pujar arxius al ftp

image::Screenshot_22.png[]

8. Configuracio de la capa de xifratge SSL

- Captura usuari i contrasenya capturades de la connexio FTP

image::Screenshot_23.png[]